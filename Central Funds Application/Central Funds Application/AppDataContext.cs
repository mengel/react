﻿using Central_Funds_Application.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace Central_Funds_Application
{
    public class AppDataContext : DbContext

    {
        public AppDataContext(DbContextOptions<AppDataContext> options)
        : base(options)
        {

        }


        public DbSet<Library> Libraries { get; set; } 
        public DbSet<CFMTrans> CFMTransactions { get; set; }
        public DbSet<CFMRoiCategory> CFMRoiCategory { get; set; }
        public DbSet<CFMDistributionType> CFMDistributionType { get; set; }
        public DbSet<CFMCategory> CFMCategory { get; set; }
        //public virtual DbSet<CFMInstitutionalPriorities> CFMInstitutionalPriorities { get; set; }
        //public object CFMInstitutionalPriorities { get; internal set; }
        public DbSet<CFMInstitutionalPriorities> CFMInstitutionalPriorities { get; set; }
        public DbSet<CFMCommitmentDetails> CFMCommitmentDetails { get; set; }
        public DbSet<CFMCommitment> CFMCommitment { get; set; }

        public DbSet<CFMCommitmentAudit> CFMCommitmentAudit { get; set; }
        public DbSet<CFMCommitmentDetailsAudit> CFMCommitmentDetailsAudit { get; set; }
        public DbSet<CFMTransAudit> CFMTransAudit { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CFMTrans>()
          .HasKey(m => new { m.ReferenceNumber, m.SequenceNumber });

            modelBuilder.Entity<CFMCategory>(entity =>
            {
                entity.HasKey("Category");

                entity.ToTable("CFM_Category");

                entity.Property(e => e.Category)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Category_Name");
            });

            modelBuilder.Entity<CFMDistributionType>(entity =>
            {
                entity.HasKey("DistributionType");

                entity.ToTable("CFM_Distribution_Type");

                entity.Property(e => e.DistributionType)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Distribution_Type");

                entity.Property(e => e.DistributionTypeName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Distribution_Type_Name");
            });

            modelBuilder.Entity<CFMRoiCategory>(entity =>
            {
                entity.HasKey("CFM_Roi_Category");

                entity.ToTable("CFM_ROI_Category");

                entity.Property(e => e.CFM_Roi_Category)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("CFM_ROI_Category");

                entity.Property(e => e.CFM_Roi_Category_Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CFM_ROI_Category_Name");
            });
            modelBuilder.Entity<CFMInstitutionalPriorities>(entity =>
            {
                entity.HasKey(e => e.CFMInstitutionalPriorities1);

                entity.ToTable("CFM_Institutional_Priorities");

                entity.Property(e => e.CFMInstitutionalPriorities1)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("CFM_Institutional_Priorities");
                entity.Property(e => e.CFMInstitutionalPrioritiesName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CFM_Institutional_Priorities_Name");
            });

            modelBuilder.Entity<CFMCommitment>().HasKey(m => new { m.ReferenceNumber, m.SequenceNumber });
            modelBuilder.Entity<CFMCommitmentDetails>().HasKey(m => new { m.ReferenceNumber, m.SequenceNumber,m.Fisicalyear });
            
            modelBuilder.Entity<CFMCommitmentDetailsAudit>().HasNoKey();
            modelBuilder.Entity<CFMCommitmentAudit>().HasNoKey();
            modelBuilder.Entity<CFMTransAudit>().HasNoKey();
            //OnModelCreatingPartial(modelBuilder);
        }

    }
}
