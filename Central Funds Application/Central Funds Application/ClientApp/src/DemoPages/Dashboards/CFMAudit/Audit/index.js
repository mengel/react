import React, { Fragment, useState, useEffect, useRef } from "react";
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { useParams } from "react-router-dom";
import DataTable from 'react-data-table-component';
import memoize from 'memoize-one';
import axios from 'axios';

import {
    Col,
    Card,
    CardBody,
    CardTitle,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Container,
} from "reactstrap";

import InvestmentModal from "../Modals/investmentModal";
import externModal from "../Modals/ModalExternal";
import InitativeForm from "../../CFMMain-Test/InitiativeForm/";

//cell: () => <button onClick={clickHandler}>Edit</button>,
const ROIcolumns = memoize(clickHandler => [
    {
        name: "Fiscal Year",
        selector: row => row.fiscalYear,
        sortable: true,
    },
    {
        name: "Projection",
        id: "projection",
        selector: row => row.projection,
        sortable: true,
    },
    {
        name: "Actuals",
        id: "actuals",
        selector: row => row.amount,
        sortable: true,
    },

]);

const institutionalPrioritiesColumns = memoize(clickHandler => [
    {
        name: "Fiscal Year",
        selector: row => row.fiscalYear,
        sortable: true,
    },
    {
        name: "Budget",
        id: "budget",
        selector: row => row.amount,
        sortable: true,
    },
    {
        name: "Actuals",
        id: "actual",
        selector: row => row.actual,
        sortable: true,
    },
    {
        name: "Remaining Balance",
        id: "balance",
        selector: row => row.projection,
        sortable: true,
    },

]);
const cfmTransAuditColumn = memoize(clickHandler => [
    {
        name: "Reference",
        selector: row => row.referenceNumber,
        sortable: true
    },
    {
        name: "Sequence",
        selector: row => row.sequenceNumber,
        sortable: true
    },
    {
        name: "Entry Date",
        selector: row => row.entryDate,
        sortable: true
    },
    {
        name: "Update Date",
        selector: row => row.updateDate,
        sortable: true
    },
    {
        name: "Last Reported Seq Date",
        selector: row => row.lastReportedSeqDate,
        sortable: true
    },
    {
        name: "Short Description",
        selector: row => row.shortDescription,
        sortable: true
    },
    {
        name: "Category",
        selector: row => row.category,
        sortable: true
    },
    {
        name: "Distribution Type",
        selector: row => row.distributionType,
        sortable: true
    },
    {
        name: "Review Flag",
        selector: row => row.reviewFlag,
        sortable: true
    },
    {
        name: "Historical Reference Number",
        selector: row => row.historicalReferenceNumber,
        sortable: true
    },
    {
        name: "Long Description",
        selector: row => row.longDescription,
        sortable: true
    },
    {
        name: "Fund",
        selector: row => row.fund,
        sortable: true
    },
    {
        name: "Total Amount",
        selector: row => row.totalAmount,
        sortable: true
    },
    {
        name: "Fiscal Year Start",
        selector: row => row.fiscalYearStart,
        sortable: true
    },
    {
        name: "Fiscal Year End",
        selector: row => row.fisicalYearEnd,
        sortable: true
    },
    {
        name: "Institutional Priorities",
        selector: row => row.institutionalPriorities,
        sortable: true
    },
    {
        name: "Institutional Priorities Explained",
        selector: row => row.institutionalPrioritiesExplained,
        sortable: true
    },
    {
        name: "Institutional Reference",
        selector: row => row.institutionalRef,
        sortable: true
    },
    {
        name: "Return on Investment",
        selector: row => row.returnOnInvestment,
        sortable: true
    },
    {
        name: "Return on Investment Description",
        selector: row => row.returnOnInvestmentDesc,
        sortable: true
    },
    {
        name: "Report Valid ROI",
        selector: row => row.reportValidRoi,
        sortable: true
    },
    {
        name: "ROI Type",
        selector: row => row.roitype,
        sortable: true
    },
    {
        name: "ROI Reference",
        selector: row => row.roiref,
        sortable: true
    },
    {
        name: "ROI",
        selector: row => row.roi,
        sortable: true
    },
    {
        name: "Report Criteria",
        selector: row => row.reportCriteria,
        sortable: true
    },
    {
        name: "Files Reference",
        selector: row => row.filesRef,
        sortable: true
    },
    {
        name: "Form Status",
        selector: row => row.formStatus,
        sortable: true
    },
    {
        name: "Decision Log Number",
        selector: row => row.decisionLogNumber,
        sortable: true
    },
    {
        name: "User",
        selector: row => row.user,
        sortable: true
    }

]);
const columns = memoize(clickHandler => [
    {
        name: "Reference",
        selector: row => row.reference,
        sortable: false,
    },
    {
        name: "sequence",
        selector: row => row.sequence,
        sortable: false,
    },
    {
        name: "Event Type",
        selector: row => row.eventType,
        sortable: false,
    },
    {
        name: "Fiscal Year",
        selector: row => row.fiscalYear,
        sortable: true,
    },
    {
        name: "Transaction Type",
        selector: row => row.transactionType,
        sortable: true,
        cell: () => <Input type="select">
            <option> Allocation</option>
            <option> Expense</option>
            <option> Revenue</option>
            <option> Financial Plans</option>
        </Input>,
    },
    {
        name: "Major Object",
        id: "majorobject",
        selector: row => row.majorObject,
        sortable: true,
    },
    {
        name: "Fund",
        id: "fund",
        selector: row => row.fund,
        sortable: true,
    },
    {
        name: "From VP",
        id: "from",
        selector: row => row.fromVP,
        sortable: true,
    },
    {
        name: "account number",
        id: "accountnumber",
        selector: row => row.accountNumber,
        sortable: true,
    },
    {
        name: "total amount",
        id: "totalamount",
        selector: row => row.fromTotalAmount,
        sortable: true,
    },
    {
        name: "To VP",
        id: "to",
        selector: row => row.toVP,
        sortable: true,
    },
    {
        name: "Account Number",
        id: "AccountNumber",
        selector: row => row.toAccountNumber,
        sortable: true,
    },
    {
        name: "Total Amount",
        id: "totalAmount",
        selector: row => row.toTotalAmount,
        sortable: true,
    }

]);

//getCFMTrans();



export function makeIPSampleData(len = 5553) {
    const sampleData = [
        {
            "reference":"123456", "sequence":"000001","eventType": "original", "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP2", "fromTotalAmount": "100", "toTotalAmount": "100"
        },
        {
            "reference": "123456", "sequence": "000002", "eventType": "update", "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP3", "fromTotalAmount": "100", "toTotalAmount": "100"
        },
        {
            "reference": "123456", "sequence": "000002", "eventType": "original", "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP3", "fromTotalAmount": "100", "toTotalAmount": "100"
        },
        {
            "reference": "123456", "sequence": "000003", "eventType": "update", "fiscalYear": "22/23","majorObject":"New Entry", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP3", "fromTotalAmount": "100", "toTotalAmount": "100"
        },
        {
            "reference": "123456", "sequence": "000003", "eventType": "original", "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP3", "fromTotalAmount": "100", "toTotalAmount": "100"
        },
        {
            "reference": "123456", "sequence": "000004", "eventType": "update", "fiscalYear": "22/23", "majorObject": "New Entry", "amount": "100", "edit": "edit", "actual": "100", "projection": "0", "fromVP": "VP1", "toVP": "VP3", "fromTotalAmount": "100", "toTotalAmount": "100", "accountNumber": "1234567", "toAccountNumber": "89012345"
        }

    ]
    return sampleData;
}

export function makeROISampleData(len = 5553) {
    const sampleData = [{
        "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0"
    }]
    return sampleData;
}
export default class FormGrid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ipdata: makeIPSampleData(),
            transAuditData: [],
            roidata: makeROISampleData(),
            roiCategoryData: [],
            categoryData: [],
            distributionData: [],
            institutionalPrioritiesData: [],
            transData: "",
            showIModal: false,
            visible: false,
            animation: "zoom",
        };
    }
    getCFMTransAudit = () => {
        axios.get("https://localhost:7216/api/CFMTransAudit/GetAll")
            .then(res => {
                this.setState({ transAuditData: [res.data[0]] })
                console.log(this.state);
            })
            .catch(error => console.log(error));
    };

    show(animation) {
        this.setState({
            animation,
            visible: true,
        });
    }

    hide() {
        this.setState({ visible: false });
    }
    showModal = () => {
        this.setState({ showIModal: true });
    };

    hideModal = () => {
        this.setState({ showIModal: false });
    };

    handleButtonClick = () => {
        console.log('clicked');
        this.setState({ standardModal: true });
    };

    handleChange = (event) => {
        const { id, value } = event.target;
        this.setState((prevState) => ({
            transAuditData: {
                ...prevState.transAuditData,
                [id]: value
            }
        }));
    };

    toggle() {
        this.setState({
            modal: !this.state.modal,
            mode: this.props.mode,
            data: this.props.data,
            ipdata: makeIPSampleData(),


        });
    }
    componentDidMount() {
        this.getCFMTransAudit();
        /*this.getAllPageCategories();*/

    }
    render() {
        let { isIniativeFormShowing } = this.state;
        let getCFMCommiment = () => {
            axios
                .get("https://localhost:7216/api/CFMTrans/GetAll")
                .then((res) => {
                    this.setState({ commitmentData: res.data });
                })
                .catch((error) => console.log(error));
        };
        let loadSelectedModel = () => {
            this.setState({ modelToLoad: this.state.currentSelectedModel[0] });
            this.setState({ isIniativeFormShowing: true });
        };
        let resetModel = () => {
            this.setState({ modelToLoad: [] });
            this.setState({ currentSelectedModel: [] });
            this.setState({ isIniativeFormShowing: false });

        }
        const handleChange = ({ selectedRows }) => {
            // You can set state or dispatch with something like Redux so we can use the retrieved data
            //console.log('Selected Rows: ', selectedRows);
            this.setState({ currentSelectedModel: selectedRows });
        };

        return (
            <Fragment>
                <TransitionGroup>
                    <CSSTransition component="div" classNames="TabsAnimation" appear={true}
                        timeout={0} enter={false} exit={false}>
                        <Container fluid>
                            <Card className="main-card mb-3">
                                <CardBody>
                                    <CardTitle>Audit</CardTitle>
                                    {!isIniativeFormShowing &&
                                        <div>
                                            <button onClick={loadSelectedModel}>View</button>
                                        </div>
                                    }
                                    {!isIniativeFormShowing && <DataTable data={this.state.transAuditData}
                                        columns={cfmTransAuditColumn(this.handleButtonClick)}
                                        selectableRows
                                        selectableRowsSingle
                                        onSelectedRowsChange={handleChange}
                                    />}
                                    {isIniativeFormShowing &&
                                        <Container fluid>
                                            <Button className="btn-icon" color="danger" onClick={resetModel}>
                                                <i className="pe-7s-back-2 btn-icon-wrapper"> </i>
                                                Back
                                            </Button>
                                        </Container>
                                    }
                                    {isIniativeFormShowing && <InitativeForm cfmModel={this.state.modelToLoad} />}
                                   
                                </CardBody>
                            </Card>
                        </Container>
                    </CSSTransition>
                </TransitionGroup>
            </Fragment>
        );
    }
}
