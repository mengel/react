import React from "react";
import {
    Col, Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import DataTable from 'react-data-table-component';
import memoize from 'memoize-one';
const columns = memoize(clickHandler => [
    {
        name: "Fiscal Year",
        selector: row => row.fiscalYear,
        sortable: true,
    },
    {
        name: "Transaction Type",
        selector: row => row.transactionType,
        sortable: true,
        cell: () => <Input type="select">
            <option> Allocation</option>
            <option> Expense</option>
            <option> Revenue</option>
            <option> Financial Plans</option>
        </Input>,
    },
    {
        name: "Major Object",
        id: "majorobject",
        selector: row => row.majorObject,
        sortable: true,
    },
    {
        name: "Fund",
        id: "fund",
        selector: row => row.fund,
        sortable: true,
    },
    {
        name: "From VP",
        id: "from",
        selector: row => row.fromAccount,
        sortable: true,
    },
    {
        name: "account number",
        id: "accountnumber",
        selector: row => row.accountNumber,
        sortable: true,
    },
    {
        name: "total amount",
        id: "totalamount",
        selector: row => row.totalAmount,
        sortable: true,
    },
    {
        name: "To VP",
        id: "to",
        selector: row => row.ToLocation,
        sortable: true,
    },
    {
        name: "Account Number",
        id: "AccountNumber",
        selector: row => row.toAccountNumber,
        sortable: true,
    },
    {
        name: "Total Amount",
        id: "totalAmount",
        selector: row => row.toAmount,
        sortable: true,
    }

]);
export function makeIPSampleData(len = 5553) {
    const sampleData = [{
        "fiscalYear": "22/23", "amount": "100", "edit": "edit", "actual": "100", "projection": "0"
    }]
    return sampleData;
}
class InvestmentModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        mode: this.props.mode,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
        modal: !this.state.modal,
        mode: this.props.mode,
        data: this.props.data,
        ipdata: makeIPSampleData(),

    });
  }

    render() {

    const buttonMode = this.state.mode;
    let button;
        if (buttonMode == "Add") {
            button = <Button className="btn-icon" color="success" onClick={this.toggle}>
                <i className="pe-7s-plus btn-icon-wrapper"> </i>
                Add
            </Button>;
        } else if (buttonMode == "Edit") {
            button = <Button className="btn-icon" color="info" onClick={this.toggle}>
                <i className="pe-7s-note btn-icon-wrapper"> </i>
                Update
            </Button>;
        }
        else {
            button = <Button className="btn-icon" color="danger" onClick={this.toggle}>
                <i className="pe-7s-close-circle btn-icon-wrapper"> </i>
                Delete
            </Button>;
        }
    return (
        <span className="d-inline-block mb-2 me-2">
            {/*<Button color="primary" onClick={this.toggle}>
                {this.state.mode}
            </Button>
            */}
        {button}
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.mode} Modal</ModalHeader>
            <ModalBody>
                    <Form>
                        <FormGroup row>
                            <Col >
                                <Label for="ReferenceNumber" >
                                    Reference Number
                                </Label>
                                <Col >
                                    <Input type="input" name="refereneceNumber" id="referenceNumber" readOnly />
                                </Col>
                            </Col>
                            <Col >
                                <Label for="sequenceNumber" >
                                    Sequence Number
                                </Label>
                                <Col >
                                    <Input type="input" name="sequenceNumber" id="sequenceNumber" readOnly />
                                </Col>
                            </Col>
                            <Col >
                                <Label for="entryDate" >
                                    Entry Date
                                </Label>
                                <Col >
                                    <Input type="input" name="entryDate" id="entryDate" readOnly />
                                </Col>
                            </Col>
                            <Col >
                                <Label for="updateDate" >
                                    Update Date
                                </Label>
                                <Col >
                                    <Input type="input" name="updateDate" id="updateDate" readOnly />
                                </Col>
                            </Col>
                            <Col >
                                <Label for="lrsDate" >
                                    Last Reported Sequence Date
                                </Label>
                                <Col >
                                    <Input type="input" name="lrsDate" id="lrsDate" readOnly />
                                </Col>
                            </Col>
                        </FormGroup>
                        <hr/>
                        <FormGroup row >
                        <Col sm={3}>
                            <Label for="fiscalYear">
                                Fiscal Year
                                </Label>
                            <Input type="text" name="fiscalYearStart" id="fiscalYearStart" placeholder="Fisical Year Start" />
                            <Input type="text" name="fiscalYearEnd" id="fiscalYearEnd" placeholder="Fisical Year End" />
                        </Col>
                        <Col sm={3}>
                            <Label for="distribionTypeLbl">
                                Distribution Type
                                </Label>
                                <Input type="select" name="distribionType" id="distribionType" readOnly >
                                    <option>Base</option>
                                    <option>Fiscal</option>
                                    <option>Recuring Fiscal</option>
                                </Input>
                            </Col>
                            
                            {/* 
                        <Col>
                            <Label for="totalApprovedLbl">
                                Total Approved
                            </Label>
                            <Input type="text" name="totalApproved" id="totalApproved" readOnly />
                        </Col>
                        <Col>
                            <Label for="totalAcctualstoDateLbl">
                                Total Actuals To Date
                            </Label>
                            <Input type="text" name="totalActualsToDate" id="totalActualsToDate" readOnly />
                        </Col>
                        <Col>
                            <Label for="totalAmountRemainingLbl">
                                Total Amount Remaining
                            </Label>
                            <Input type="text" name="totalAmountRemaining" id="totalAmountRemaining" readOnly />
                        </Col>
                        <Col>
                            <Label for="totalEstimatePerYearLbl">
                                Total Estimate per Year
                            </Label>
                            <Input type="text" name="totalEstimatePerYear" id="totalEstimatePerYear" readOnly />
                        </Col>
                       */}
                        </FormGroup>
                        
                        <FormGroup>
                            <Col sm={3}>
                            <Label for="budgetActualeLBL">
                                Please Select Budget/Actual
                            </Label>
                            <Input type="select" name="budgetType" id="budgetType" readOnly >
                                <option>Budget</option>
                                <option>Actual</option>
                                </Input>
                            </Col>
                        </FormGroup>
                    </Form>
                    <Col sm={12}>
                    <DataTable data={this.state.ipdata}
                        columns={columns(this.handleButtonClick)}
                        />
                    </Col>
                    <FormGroup row>
                        <Col sm={6}>
                        </Col>
                        <Col sm={2}>
                            <Label for="totalFromLBL">
                                Total From
                            </Label>
                            <Input type="text" name="totalFrom" id="totalFrom" readOnly />
                        </Col>
                        <Col sm={2}>
                            <Label for="totalToLBL">
                                Total To
                            </Label>
                            <Input type="text" name="totalTo" id="totalTo" readOnly />
                        </Col>
                    </FormGroup>
            
          </ModalBody>
          <ModalFooter>
            <Button color="link" onClick={this.toggle}>
              Cancel
            </Button>
            <Button color="primary" onClick={this.toggle}>
              Submit
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </span>
    );
  }
}

export default InvestmentModal;
