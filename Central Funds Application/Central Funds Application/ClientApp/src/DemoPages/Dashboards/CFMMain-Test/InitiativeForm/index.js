import React, { Fragment, useState, useEffect, useRef } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { useParams } from "react-router-dom";
import DataTable from "react-data-table-component";
import memoize from "memoize-one";
import axios from "axios";

import {
    Col,
    Card,
    CardBody,
    CardTitle,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Container,
} from "reactstrap";

import InvestmentModal from "../Modals/investmentModal";
import externModal from "../Modals/ModalExternal";

//cell: () => <button onClick={clickHandler}>Edit</button>,
const ROIcolumns = memoize((clickHandler) => [
    {
        name: "Fiscal Year",
        selector: (row) => row.fiscalYear,
        sortable: true,
    },
    {
        name: "Projection",
        id: "projection",
        selector: (row) => row.projection,
        sortable: true,
    },
    {
        name: "Actuals",
        id: "actuals",
        selector: (row) => row.amount,
        sortable: true,
    },
]);

const institutionalPrioritiesColumns = memoize((clickHandler) => [
    {
        name: "Fiscal Year start",
        selector: (row) => row.fiscalYearStart,
        sortable: true,
    },
    {
        name: "Fiscal Year end",
        selector: (row) => row.fiscalYearEnd,
        sortable: true,
    },
    {
        name: "Budget",
        id: "budget",
        selector: (row) => row.amount,
        sortable: true,
    },
    {
        name: "Actuals",
        id: "actual",
        selector: (row) => row.actual,
        sortable: true,
    },
    {
        name: "Remaining Balance",
        id: "balance",
        selector: (row) => row.projection,
        sortable: true,
    },
]);

//getCFMTrans();

export function makeIPSampleData(len = 5553) {
    const sampleData = [
        {
            fiscalYear: "22/23",
            amount: "100",
            edit: "edit",
            actual: "100",
            projection: "0",
        },
    ];
    return sampleData;
}
export function makeROISampleData(len = 5553) {
    const sampleData = [
        {
            fiscalYear: "22/23",
            amount: "100",
            edit: "edit",
            actual: "100",
            projection: "0",
        },
    ];
    return sampleData;
}
export default class FormGrid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ipdata: makeIPSampleData(),
            roidata: makeROISampleData(),
            commitmentData: [],
            roiCategoryData: [],
            categoryData: [],
            distributionData: [],
            institutionalPrioritiesData: [],
            transData: props.cfmModel ? props.cfmModel : [],
            showIModal: false,
            visible: false,
            animation: "zoom",
        };
        console.log(props.cfmModel);
        //this.transData = this.getCFMTrans.bind(this);
        // this.showModal = this.showModal.bind(this);
        //this.hideModal = this.hideModal.bind(this);
    }
    //const[transData, setTransData] = useState(0);

    getCFMTrans = () => {
        axios
            .get("https://localhost:7216/api/CFMTrans/GetAll")
            .then((res) => {
                this.setState({ transData: res.data[0] });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));
    };
    submitCFMTrans = () => {
        axios
            .put("https://localhost:7216/api/CFMTrans/Update", this.state.transData)
            .then((response) => console.log(response));
    };
    getCFMCommiment = () => {
        axios
            .get("https://localhost:7216/api/CFMTrans/GetAllCommitment")
            .then((res) => {
                this.setState({ commitmentData: [res.data[0]] });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));
    };

    getAllPageCategories = () => {
        axios
            .get("https://localhost:7216/api/CFMCategories/GetAllROICategories")
            .then((res) => {
                this.setState({ roiCategoryData: res.data });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));

        axios
            .get("https://localhost:7216/api/CFMCategories/GetAllCategories")
            .then((res) => {
                this.setState({ categoryData: res.data });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));

        axios
            .get("https://localhost:7216/api/CFMCategories/GetAllDistributionType")
            .then((res) => {
                this.setState({ distributionData: res.data });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));

        axios
            .get(
                "https://localhost:7216/api/CFMCategories/GetAllInstitutionalPriorities"
            )
            .then((res) => {
                this.setState({ institutionalPrioritiesData: res.data });
                //console.log(this.state);
            })
            .catch((error) => console.log(error));
    };

    show(animation) {
        this.setState({
            animation,
            visible: true,
        });
    }

    hide() {
        this.setState({ visible: false });
    }
    showModal = () => {
        this.setState({ showIModal: true });
    };

    hideModal = () => {
        this.setState({ showIModal: false });
    };

    handleButtonClick = () => {
       // console.log("clicked");
        this.setState({ standardModal: true });
    };

    handleChange = (event) => {
        const { id, value } = event.target;
        this.setState((prevState) => ({
            transData: {
                ...prevState.transData,
                [id]: value,
            },
        }));
    };
    /* handleChangeInSelectedRow = state => {
          console.log('state', state.selectedRows);
                this.setState({ selectedRows: state.selectedRows });
      };*/
    componentDidMount() {
        //this.getCFMTrans();
        this.getAllPageCategories();
        this.getCFMCommiment();
    }
    render() {
        const { transData } = this.state;

        return (
            <Fragment>
                <TransitionGroup>
                    <CSSTransition
                        component="div"
                        classNames="TabsAnimation"
                        appear={true}
                        timeout={0}
                        enter={false}
                        exit={false}
                    >
                        <Container fluid>
                            <Card className="main-card mb-3">
                                <CardBody>
                                    <CardTitle>New Initative</CardTitle>
                                    <FormGroup row>
                                        <Col>
                                            <Label for="ReferenceNumber">Reference Number</Label>
                                            <Col>
                                                <Input
                                                    value={this.state.transData.referenceNumber}
                                                    onChange={this.handleChange}
                                                    className="form-control"
                                                    onChange={this.handleChange}
                                                    type="input"
                                                    name="refereneceNumber"
                                                    id="referenceNumber"
                                                    readOnly
                                                />
                                            </Col>
                                        </Col>
                                        <Col>
                                            <Label for="sequenceNumber">Sequence Number</Label>
                                            <Col>
                                                <Input
                                                    value={this.state.transData.sequenceNumber || ""}
                                                    onChange={this.handleChange}
                                                    type="input"
                                                    name="sequenceNumber"
                                                    id="sequenceNumber"
                                                    readOnly
                                                />
                                            </Col>
                                        </Col>
                                        <Col>
                                            <Label for="entryDate">Entry Date</Label>
                                            <Col>
                                                <Input
                                                    value={this.state.transData.entryDate || ""}
                                                    onChange={this.handleChange}
                                                    type="input"
                                                    name="entryDate"
                                                    id="entryDate"
                                                    readOnly
                                                />
                                            </Col>
                                        </Col>
                                        <Col>
                                            <Label for="updateDate">Update Date</Label>
                                            <Col>
                                                <Input
                                                    value={this.state.transData.updateDate || ""}
                                                    onChange={this.handleChange}
                                                    type="input"
                                                    name="updateDate"
                                                    id="updateDate"
                                                    readOnly
                                                />
                                            </Col>
                                        </Col>
                                        <Col>
                                            <Label for="lrsDate">Last Reported Sequence Date</Label>
                                            <Col>
                                                <Input
                                                    value={this.state.transData.lastReportedSeqDate || ""}
                                                    onChange={this.handleChange}
                                                    type="input"
                                                    name="lrsDate"
                                                    id="lrsDate"
                                                    readOnly
                                                />
                                            </Col>
                                        </Col>
                                    </FormGroup>

                                    <FormGroup row></FormGroup>
                                    <hr />

                                    <Form>
                                        <FormGroup row>
                                            <Col>
                                                <Label for="shortDescription">Short Description</Label>
                                                <Col>
                                                    <Input
                                                        onChange={this.handleChange}
                                                        type="input"
                                                        value={this.state.transData.shortDescription || ""}
                                                        name="shortDescription"
                                                        id="shortDescription"
                                                        placeholder="Short Description"
                                                    />
                                                </Col>
                                            </Col>
                                            <Col>
                                                <Label for="category">Category</Label>
                                                <Col>
                                                    <Input
                                                        value={this.state.transData.category || ""}
                                                        onChange={this.handleChange}
                                                        type="select"
                                                        name="category"
                                                        id="category"
                                                    >
                                                        {this.state.categoryData.map((cat) => (
                                                            <option value={cat.category}>
                                                                {cat.categoryName}
                                                            </option>
                                                        ))}
                                                    </Input>
                                                </Col>
                                            </Col>
                                            <Col>
                                                <Label for="distributionType">Distribution Type</Label>
                                                <Col>
                                                    <Input
                                                        value={this.state.transData.distributionType || ""}
                                                        onChange={this.handleChange}
                                                        type="select"
                                                        name="distributionType"
                                                        id="distributionType"
                                                    >
                                                        {this.state.distributionData.map((dist) => (
                                                            <option value={dist.distributionType}>
                                                                {dist.distributionTypeName}
                                                            </option>
                                                        ))}
                                                    </Input>
                                                </Col>
                                            </Col>

                                            <Col sm={1}>
                                                <Label for="reviewFlag">Review Flag</Label>
                                                <Col sm={2}>
                                                    <Input
                                                        value={this.state.transData.reviewFlag || ""}
                                                        type="checkbox"
                                                        id="reviewFlag"
                                                    />
                                                </Col>
                                            </Col>

                                            <Col>
                                                <Label for="histRefNumbers">
                                                    Historical Reference Numbers
                                                </Label>
                                                <Col>
                                                    <Input
                                                        value={
                                                            this.state.transData.historicalReferenceNumber ||
                                                            ""
                                                        }
                                                        onChange={this.handleChange}
                                                        type="input"
                                                        name="historicalReferenceNumber"
                                                        id="historicalReferenceNumber"
                                                    />
                                                </Col>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="longDescription" sm={12}>
                                                Description
                                            </Label>
                                            <Col sm={12}>
                                                <Input
                                                    value={this.state.transData.longDescription || ""}
                                                    onChange={this.handleChange}
                                                    type="textarea"
                                                    name="longDescription"
                                                    id="longDescription"
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup row>
                                            <Col>
                                                <FormGroup row>
                                                    <Col sm={4}>
                                                        <Label for="fiscalYearStart">
                                                            Fiscal Year Start
                                                        </Label>
                                                        <Col>
                                                            <Input
                                                                value={
                                                                    this.state.transData.fiscalYearStart || ""
                                                                }
                                                                onChange={this.handleChange}
                                                                type="select"
                                                                name="fiscalYearStart"
                                                                id="fiscalYearStart"
                                                            >
                                                                <option>22/23</option>
                                                            </Input>
                                                        </Col>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Label for="fiscalYearEnd">Fiscal Year End</Label>
                                                        <Col>
                                                            <Input
                                                                value={
                                                                    this.state.transData.fisicalYearEnd || ""
                                                                }
                                                                onChange={this.handleChange}
                                                                type="select"
                                                                name="fiscalYearEnd"
                                                                id="fiscalYearEnd"
                                                            >
                                                                <option>23/24</option>
                                                            </Input>
                                                        </Col>
                                                    </Col>
                                                </FormGroup>
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row></FormGroup>

                                        <hr />
                                        <FormGroup row>
                                            <Col md={6}>
                                                <FormGroup row>
                                                    <Label for="roiLabel" sm={12}>
                                                        Institutional Priorities:
                                                    </Label>
                                                </FormGroup>

                                                <FormGroup row>
                                                    <Col>
                                                        <Label for="IP" sm={12}>
                                                            Institutional Priorities
                                                        </Label>
                                                        <FormGroup row>
                                                            <Col sm={12}>
                                                                <Input
                                                                    value={
                                                                        this.state.transData.fisicalYearEnd || ""
                                                                    }
                                                                    onChange={this.handleChange}
                                                                    type="select"
                                                                    name="IP"
                                                                    id="IP"
                                                                >
                                                                    {this.state.institutionalPrioritiesData.map(
                                                                        (IP) => (
                                                                            <option
                                                                                value={IP.cfmInstitutionalPriorities1}
                                                                            >
                                                                                {IP.cfmInstitutionalPrioritiesName}
                                                                            </option>
                                                                        )
                                                                    )}
                                                                </Input>
                                                            </Col>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <Label for="ipe" sm={12}>
                                                            Institutional Priorities Explained
                                                        </Label>
                                                        <FormGroup row>
                                                            <Col sm={12}>
                                                                <Input
                                                                    type="text"
                                                                    name="ipeDescription"
                                                                    id="ipeDescription"
                                                                />
                                                            </Col>
                                                        </FormGroup>
                                                    </Col>
                                                </FormGroup>
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Col>
                                                <InvestmentModal
                                                    mode={"Add"}
                                                    data={ "add"}

                                                />
                                                <InvestmentModal
                                                    mode={"Edit"}
                                                    data={this.state.commitmentData}
                                                />
                                                <InvestmentModal
                                                    mode={"Delete"}
                                                    data={this.state.commitmentData}
                                                />
                                                
                                                <DataTable
                                                    data={this.state.commitmentData}
                                                    columns={institutionalPrioritiesColumns(
                                                        this.handleButtonClick
                                                    )}
                                                    selectableRows
                                                    selectableRowsSingle
                                                />
                                            </Col>
                                        </FormGroup>
                                        <hr />
                                        <FormGroup row>
                                            <Col md={6}>
                                                <FormGroup row>
                                                    <Label for="roiLabel" sm={6}>
                                                        Return on Investment:
                                                    </Label>
                                                </FormGroup>

                                                <FormGroup row>
                                                    <Col md={6}>
                                                        <Label for="roi" sm={12}>
                                                            Return on Investment
                                                        </Label>
                                                        <FormGroup row>
                                                            <Col sm={12}>
                                                                <Input
                                                                    value={
                                                                        this.state.transData.cfM_Roi_Category || ""
                                                                    }
                                                                    onChange={this.handleChange}
                                                                    type="select"
                                                                    name="roi"
                                                                    id="roi"
                                                                >
                                                                    {this.state.roiCategoryData.map((cat) => (
                                                                        <option value={cat.cfM_Roi_Category}>
                                                                            {cat.cfM_Roi_Category_Name}
                                                                        </option>
                                                                    ))}
                                                                </Input>
                                                            </Col>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md={6}>
                                                        <Label for="roiDescription" sm={12}>
                                                            Return of Investment Description
                                                        </Label>
                                                        <FormGroup row>
                                                            <Col sm={12}>
                                                                <Input
                                                                    type="text"
                                                                    name="roiDescription"
                                                                    id="roiDescription"
                                                                />
                                                            </Col>
                                                        </FormGroup>
                                                    </Col>
                                                </FormGroup>
                                                <FormGroup row>
                                                    <Col>
                                                        <Label for="RUVROI">
                                                            {" "}
                                                            Report used to Validate ROI
                                                        </Label>
                                                    </Col>
                                                    <Col>
                                                        <Input type="text" name="RUIVROI" id="RUVROI" />
                                                    </Col>
                                                </FormGroup>
                                                <FormGroup row>
                                                    <Col>
                                                        <Label for="QualQuantROI">
                                                            Is ROI Qualitative or Quantitive?
                                                        </Label>
                                                    </Col>
                                                    <Col>
                                                        <Input
                                                            value={this.state.transData.QualQuantROI || ""}
                                                            onChange={this.handleChange}
                                                            type="select"
                                                            name="roi"
                                                            id="QualQuantROI"
                                                        >
                                                            <option>Qualitative</option>
                                                            <option>Quantitive</option>
                                                        </Input>
                                                    </Col>
                                                </FormGroup>
                                            </Col>
                                            <FormGroup row></FormGroup>
                                            <FormGroup row>
                                                <Col>
                                                    <FormGroup row>
                                                        <Col>
                                                            <InvestmentModal
                                                                mode={"Add"}
                                                                data={this.state.roidata}
                                                            />
                                                            <InvestmentModal
                                                                mode={"Edit"}
                                                                data={this.state.roidata}
                                                            />
                                                            <InvestmentModal
                                                                mode={"Delete"}
                                                                data={this.state.roidata}
                                                            />
                                                            <DataTable
                                                                data={this.state.roidata}
                                                                columns={ROIcolumns(this.handleButtonClick)}
                                                                selectableRows
                                                                selectableRowsSingle
                                                            />
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row></FormGroup>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Label for="ROIExplained">
                                                                RETURN ON INVESTMENT (EXPLAIN MEASURABLE
                                                                OUTCOMES):
                                                            </Label>
                                                            <Input
                                                                type="text"
                                                                name="ROIExplained"
                                                                id="ROIExplained"
                                                            />
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Label for="ROIReportCriteria">
                                                                Report Criteria:
                                                            </Label>
                                                            <Input
                                                                type="text"
                                                                name="ROIReportCriteria"
                                                                id="ROIReportCriteria"
                                                            />
                                                        </Col>
                                                    </FormGroup>
                                                </Col>
                                            </FormGroup>
                                        </FormGroup>

                                        <hr />
                                        <FormGroup row>
                                            <Col>
                                                <Label for="exampleFile">File</Label>
                                                <Col>
                                                    <Input type="file" name="file" id="exampleFile" />
                                                </Col>
                                            </Col>
                                            <Col>
                                                <Label for="statusOfForm">Status Of Form:</Label>
                                                <Input
                                                    type="text"
                                                    name="statusOfForm"
                                                    id="statusOfFOrm"
                                                />
                                            </Col>
                                            <Col>
                                                <Label for="cafNumber"> CAF Number:</Label>
                                                <Input type="text" name="cafNumber" id="cafNumber" />
                                            </Col>
                                            <Col>
                                                <Label for="decisionLogNumber">
                                                    {" "}
                                                    Decision Log Number:
                                                </Label>
                                                <Input
                                                    type="text"
                                                    name="decisionLogNumber"
                                                    id="decisionLogNumber"
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup floating row>
                                            <Col>
                                                <Button
                                                    className="float-right"
                                                    onClick={this.submitCFMTrans}
                                                >
                                                    Submit
                                                </Button>
                                            </Col>
                                        </FormGroup>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Container>
                    </CSSTransition>
                </TransitionGroup>
            </Fragment>
        );
    }
}
