import React, { Component, Fragment,useState } from "react";
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import DataTable from "react-data-table-component";
import memoize from "memoize-one";
import axios from "axios";

import {
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Container,
} from "reactstrap";

import PageTitle from "../../../Layout/AppMain/PageTitle";

import Tabs, { TabPane } from "rc-tabs";
import TabContent from "rc-tabs/lib/SwipeableTabContent";
import ScrollableInkTabBar from "rc-tabs/lib/ScrollableInkTabBar";
import InitativeForm from "./InitiativeForm/";

const CFMColumns = memoize((clickHandler) => [
    {
        name: "Short Description",
        selector: (row) => row.shortDescription,
        sortable: true,
    },
    {
        name: "Reference Number",
        selector: (row) => row.referenceNumber,
        sortable: true,
    },
    {
        name: "Sequence Number",
        selector: (row) => row.sequenceNumber,
        sortable: true,
    },
    {
        name: "Fiscal Year Start",
        selector: (row) => row.fiscalYearStart,
        sortable: true,
    },
    {
        name: "Fiscal Year End",
        selector: (row) => row.fisicalYearEnd,
        sortable: true,
    },
]);

export default class AnalyticsDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isIniativeFormShowing: false,
            currentSelectedModel: [],
            modelToLoad: [],
        };
    }   

    render() {

        return (
            <Fragment>
                <TransitionGroup>
                    <CSSTransition component="div" classNames="TabsAnimation" appear={true}
                        timeout={1500} enter={false} exit={false}>
                        <div>
                            {/** <PageTitle heading="Initiative Dashboard"
                                subheading="This is where CFM initatives will be created"
                                icon="pe-7s-note2 icon-gradient bg-mean-fruit" /> **/}
                            
                            <InitativeForm cfmModel={this.state.modelToLoad} />
                        </div>
                    </CSSTransition>
                </TransitionGroup>
            </Fragment>
        );
    }
}
