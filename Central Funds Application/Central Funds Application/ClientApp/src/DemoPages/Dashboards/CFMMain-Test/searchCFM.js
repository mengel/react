import React, { Component, Fragment,useState } from "react";
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import DataTable from "react-data-table-component";
import memoize from "memoize-one";
import axios from "axios";

import {
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Container,
} from "reactstrap";

import PageTitle from "../../../Layout/AppMain/PageTitle";

import Tabs, { TabPane } from "rc-tabs";
import TabContent from "rc-tabs/lib/SwipeableTabContent";
import ScrollableInkTabBar from "rc-tabs/lib/ScrollableInkTabBar";
import InitativeForm from "./InitiativeForm/";

const CFMColumns = memoize((clickHandler) => [
    {
        name: "Short Description",
        selector: (row) => row.shortDescription,
        sortable: true,
    },
    {
        name: "Reference Number",
        selector: (row) => row.referenceNumber,
        sortable: true,
    },
    {
        name: "Sequence Number",
        selector: (row) => row.sequenceNumber,
        sortable: true,
    },
    {
        name: "Fiscal Year Start",
        selector: (row) => row.fiscalYearStart,
        sortable: true,
    },
    {
        name: "Fiscal Year End",
        selector: (row) => row.fisicalYearEnd,
        sortable: true,
    },
]);

export default class AnalyticsDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isIniativeFormShowing: false,
            currentSelectedModel: [],
            modelToLoad: [],
        };
    }   

    render() {
        let { isIniativeFormShowing } = this.state;
        let getCFMCommiment = () => {
            axios
                .get("https://localhost:7216/api/CFMTrans/GetAll")
                .then((res) => {
                    this.setState({ commitmentData: res.data });
                })
                .catch((error) => console.log(error));
        };
        let loadSelectedModel = () => {
            this.setState({ modelToLoad: this.state.currentSelectedModel[0] });
            this.setState({ isIniativeFormShowing: true });
        };
        let resetModel = () => {
            this.setState({ modelToLoad: [] });
            this.setState({ currentSelectedModel: [] });
            this.setState({ isIniativeFormShowing: false });

        }
        const handleChange = ({ selectedRows }) => {
            // You can set state or dispatch with something like Redux so we can use the retrieved data
            //console.log('Selected Rows: ', selectedRows);
            this.setState({ currentSelectedModel: selectedRows });
        };

        return (
            <Fragment>
                <TransitionGroup>
                    <CSSTransition component="div" classNames="TabsAnimation" appear={true}
                        timeout={1500} enter={false} exit={false}>
                        <div>
                            {/** <PageTitle heading="Initiative Dashboard"
                                subheading="This is where CFM initatives will be created"
                                icon="pe-7s-note2 icon-gradient bg-mean-fruit" /> **/}
                            
                            {!isIniativeFormShowing &&
                                <div class="main-card card" >
                                    <div class="card-body">
                                        <row>
                                            <Col md={3}>
                                                <select class="mb-2 form-select-sm form-select">
                                                    <option>Reference number</option>
                                                    <option>Description</option>
                                                    <option>Fund</option>
                                                    <option>VP Area</option>
                                                </select>
                                            </Col>
                                            <Col md={6}>
                                                <input
                                                type="text"
                                                placeholder="Search..."
                                                class="form-control"
                                                />
                                            </Col>
                                            <Col md={2}>
                                                <button onClick={getCFMCommiment}>Search</button>
                                            </Col>
                                       </row>
                                    </div>
                                
                                </div>
                            }

                            {!isIniativeFormShowing &&
                                <div>
                                    <button onClick={loadSelectedModel}>View</button>
                                </div>
                            }
                            
                            {!isIniativeFormShowing &&
                                <DataTable
                                data={this.state.commitmentData}
                                columns={CFMColumns(
                                    this.handleButtonClick
                                )}
                                selectableRows
                                selectableRowsSingle
                                    onSelectedRowsChange={handleChange} />
                            }
                            {isIniativeFormShowing &&
                                <Container fluid>
                                    <Button className="btn-icon" color="danger" onClick={resetModel}>
                                        <i className="pe-7s-back-2 btn-icon-wrapper"> </i>
                                        Back
                                    </Button>
                                </Container>
                            }
                            {isIniativeFormShowing && <InitativeForm cfmModel={this.state.modelToLoad} />}
                        </div>
                    </CSSTransition>
                </TransitionGroup>
            </Fragment>
        );
    }
}
