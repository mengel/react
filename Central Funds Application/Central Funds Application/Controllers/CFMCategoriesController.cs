﻿using Central_Funds_Application.Model.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Central_Funds_Application.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class CFMCategoriesController : ControllerBase
{
    private readonly ICFMRoiCategoryService _CFMRoiCategoryService;
    private readonly ICFMCategoryService _CFMCategoryService;
    private readonly ICFMDistributionTypeService _CFMDistributionTypeService;
    private readonly ICFMInstitutionalPrioritiesService _CFMInstitutionalPrioritiesService;



    /*  ICFMCategoryService prICFMCategory*/
    public CFMCategoriesController(ICFMRoiCategoryService prICFMRoiCategoryService, ICFMCategoryService prICFMCategoryService, ICFMDistributionTypeService prICFMDistributionTypeService, ICFMInstitutionalPrioritiesService cFMInstitutionalPrioritiesService)
    {
        _CFMRoiCategoryService = prICFMRoiCategoryService;
        _CFMCategoryService = prICFMCategoryService;
        _CFMDistributionTypeService = prICFMDistributionTypeService;
        _CFMInstitutionalPrioritiesService = cFMInstitutionalPrioritiesService;
    }

    [HttpGet]
    public IActionResult GetAllROICategories()
    {
        List<CFMRoiCategory> lResult = _CFMRoiCategoryService.GetAll();
        return Ok(lResult);
    }
    
    [HttpGet]
    public IActionResult GetAllCategories()
    {
        List<CFMCategory> lResult = _CFMCategoryService.GetAll();
        return Ok(lResult);
    }
    
    [HttpGet]
    public IActionResult GetAllDistributionType()
    {
        List<CFMDistributionType> lResult = _CFMDistributionTypeService.GetAll();
        return Ok(lResult);
    }

    [HttpGet]
    public IActionResult GetAllInstitutionalPriorities()
    {
        List<CFMInstitutionalPriorities> lResult = _CFMInstitutionalPrioritiesService.GetAll();
        return Ok(lResult);
    }

    [HttpGet]
    public IActionResult getAllCategorySettings()
    {
        List<CFMRoiCategory> roi = _CFMRoiCategoryService.GetAll();
        List<CFMCategory> cat = _CFMCategoryService.GetAll();
        List<CFMDistributionType> dist = _CFMDistributionTypeService.GetAll();
        ListHelper myList = new ListHelper();
        myList.DistributionTypes = dist;
        myList.Category = cat;
        myList.ROICategory = roi;

        return Ok(myList);
    }

    /*[HttpGet]
    public IActionResult GetAllCategories()
    {
        List<CFMCategory> lResult = _CFMCategoryService.GetAll();
        return Ok(lResult);
    }*/

}

public class ListHelper
{
    public List<CFMRoiCategory> ROICategory;
    public List<CFMCategory> Category;
    public List<CFMDistributionType> DistributionTypes;
    public string type;
}