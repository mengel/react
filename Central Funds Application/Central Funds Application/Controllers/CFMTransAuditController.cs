﻿using Central_Funds_Application.Model.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Central_Funds_Application.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class CFMTransAuditController : ControllerBase
{
    private readonly ICFMTransAuditService _ICFMTransAuditAuditService;
    private readonly ICFMCommitmentAuditService _ICFMCommitmentAuditService;
    private readonly ICFMCommitmentDetailsAuditService _ICFMCommitmentDetailAuditService;
    public CFMTransAuditController(ICFMTransAuditService prICFMTransAuditAuditService, ICFMCommitmentAuditService prCFMCommitmentAuditService, ICFMCommitmentDetailsAuditService iCFMCommitmentDetailAuditService)
    {
        _ICFMTransAuditAuditService = prICFMTransAuditAuditService;
        _ICFMCommitmentAuditService = prCFMCommitmentAuditService;
        _ICFMCommitmentDetailAuditService = iCFMCommitmentDetailAuditService;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        List<CFMTransAudit> lResult = _ICFMTransAuditAuditService.GetAll();
        return Ok(lResult);
    }
    /*
    [HttpGet]
    public IActionResult Search(string prName = "")
    {
        List<CFMTransAudit> lResult = _ICFMTransAuditAuditService.GetByName(prName);
        return Ok(lResult);
    }
    */

    [HttpPut]
    public IActionResult Update(CFMTransAudit prCFMTransAudit)
    {
        return Ok(_ICFMTransAuditAuditService.Update(prCFMTransAudit));
    }

    [HttpPost]
    public IActionResult Save(CFMTransAudit prCFMTransAudit)
    {
        return Ok(_ICFMTransAuditAuditService.Save(prCFMTransAudit));
    }

    [HttpDelete]
    public IActionResult Delete(CFMTransAudit prCFMTransAudit)
    {
        _ICFMTransAuditAuditService.Delete(prCFMTransAudit);
        return Ok();
    }


    [HttpGet]
    public IActionResult GetAllCommitment()
    {
        List<CFMCommitmentAudit> lResult = _ICFMCommitmentAuditService.GetAll();
        return Ok(lResult);
    }

    [HttpGet]
    public IActionResult GetAllCommitmentDetails()
    {
        List<CFMCommitmentDetailsAudit> lResult = _ICFMCommitmentDetailAuditService.GetAll();
        return Ok(lResult);
    }


}