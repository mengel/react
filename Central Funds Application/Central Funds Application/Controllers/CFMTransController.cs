﻿using Central_Funds_Application.Model.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Central_Funds_Application.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class CFMTransController : ControllerBase
{
    private readonly ICFMTransService _ICFMTransService;
    private readonly ICFMCommitmentService _ICFMCommitmentService;
    private readonly ICFMCommitmentDetailService _ICFMCommitmentDetailService;
    public CFMTransController(ICFMTransService prICFMTransService, ICFMCommitmentService prCFMCommitmentService, ICFMCommitmentDetailService iCFMCommitmentDetailService)
    {
        _ICFMTransService = prICFMTransService;
        _ICFMCommitmentService = prCFMCommitmentService;
        _ICFMCommitmentDetailService = iCFMCommitmentDetailService;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        List<CFMTrans> lResult = _ICFMTransService.GetAll();
        return Ok(lResult);
    }

    /*[HttpGet]
    public IActionResult Search(string prName = "")
    {
        List<CFMTrans> lResult = _ICFMTransService.GetByName(prName);
        return Ok(lResult);
    }*/

    [HttpPut]
    public IActionResult Update(CFMTrans prCFMTrans)
    {
        return Ok(_ICFMTransService.Update(prCFMTrans));
    }

    [HttpPost]
    public IActionResult Save(CFMTrans prCFMTrans)
    {
        return Ok(_ICFMTransService.Save(prCFMTrans));
    }

    [HttpDelete]
    public IActionResult Delete(CFMTrans prCFMTrans)
    {
        _ICFMTransService.Delete(prCFMTrans);
        return Ok();
    }


    [HttpGet]
    public IActionResult GetAllCommitment()
    {
        List<CFMCommitment> lResult = _ICFMCommitmentService.GetAll();
        return Ok(lResult);
    }

    [HttpGet]
    public IActionResult GetAllCommitmentDetails()
    {
        List<CFMCommitmentDetails> lResult = _ICFMCommitmentDetailService.GetAll();
        return Ok(lResult);
    }


}