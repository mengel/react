﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Central_Funds_Application.Migrations
{
    /// <inheritdoc />
    public partial class Audit : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CFM_Category",
                columns: table => new
                {
                    Category = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    Category_Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Category", x => x.Category);
                });

            migrationBuilder.CreateTable(
                name: "CFM_Commitment",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FiscalYearStart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FiscalYearEnd = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DistributionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BudgetActualFlag = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Commitment", x => new { x.ReferenceNumber, x.SequenceNumber });
                });

            migrationBuilder.CreateTable(
                name: "CFM_Commitment_Audit",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FiscalYearStart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FiscalYearEnd = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DistributionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BudgetActualFlag = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "CFM_Commitment_Details",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Fisicalyear = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TransactionTyepe = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Fund = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FromVP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FromAccountNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalAmount = table.Column<int>(type: "int", nullable: false),
                    toVP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ToAccountNumber = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Commitment_Details", x => new { x.ReferenceNumber, x.SequenceNumber, x.Fisicalyear });
                });

            migrationBuilder.CreateTable(
                name: "CFM_Commitment_Details_Audit",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Fisicalyear = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TransactionTyepe = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Fund = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FromVP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FromAccountNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalAmount = table.Column<int>(type: "int", nullable: false),
                    toVP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ToAccountNumber = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "CFM_Distribution_Type",
                columns: table => new
                {
                    Distribution_Type = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    Distribution_Type_Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Distribution_Type", x => x.Distribution_Type);
                });

            migrationBuilder.CreateTable(
                name: "CFM_Institutional_Priorities",
                columns: table => new
                {
                    CFM_Institutional_Priorities = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    CFM_Institutional_Priorities_Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Institutional_Priorities", x => x.CFM_Institutional_Priorities);
                });

            migrationBuilder.CreateTable(
                name: "CFM_ROI_Category",
                columns: table => new
                {
                    CFM_ROI_Category = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    CFM_ROI_Category_Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_ROI_Category", x => x.CFM_ROI_Category);
                });

            migrationBuilder.CreateTable(
                name: "CFM_Trans",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastReportedSeqDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShortDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DistributionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReviewFlag = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    HistoricalReferenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LongDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Fund = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalAmount = table.Column<int>(type: "int", nullable: true),
                    FiscalYearStart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FisicalYearEnd = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalPriorities = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalPrioritiesExplained = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalRef = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReturnOnInvestment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReturnOnInvestmentDesc = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportValidRoi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roitype = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roiref = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportCriteria = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FilesRef = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FormStatus = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DecisionLogNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    User = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CFM_Trans", x => new { x.ReferenceNumber, x.SequenceNumber });
                });

            migrationBuilder.CreateTable(
                name: "CFM_Trans_Audit",
                columns: table => new
                {
                    ReferenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SequenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastReportedSeqDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShortDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DistributionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReviewFlag = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    HistoricalReferenceNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LongDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Fund = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalAmount = table.Column<int>(type: "int", nullable: true),
                    FiscalYearStart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FisicalYearEnd = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalPriorities = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalPrioritiesExplained = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    InstitutionalRef = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReturnOnInvestment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReturnOnInvestmentDesc = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportValidRoi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roitype = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roiref = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportCriteria = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FilesRef = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FormStatus = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DecisionLogNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    User = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "Library",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CFM_Category");

            migrationBuilder.DropTable(
                name: "CFM_Commitment");

            migrationBuilder.DropTable(
                name: "CFM_Commitment_Audit");

            migrationBuilder.DropTable(
                name: "CFM_Commitment_Details");

            migrationBuilder.DropTable(
                name: "CFM_Commitment_Details_Audit");

            migrationBuilder.DropTable(
                name: "CFM_Distribution_Type");

            migrationBuilder.DropTable(
                name: "CFM_Institutional_Priorities");

            migrationBuilder.DropTable(
                name: "CFM_ROI_Category");

            migrationBuilder.DropTable(
                name: "CFM_Trans");

            migrationBuilder.DropTable(
                name: "CFM_Trans_Audit");

            migrationBuilder.DropTable(
                name: "Library");
        }
    }
}
