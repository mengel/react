﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMCategoryService : ICFMCategoryService
{
    public AppDataContext _context { get; set; }
    public CFMCategoryService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMCategory> GetAll()
    {
        return _context.CFMCategory.ToList();
    }
}