﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMCommitmentAuditService : ICFMCommitmentAuditService
{
    public AppDataContext _context { get; set; }
    public CFMCommitmentAuditService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMCommitmentAudit> GetAll()
    {
        return _context.CFMCommitmentAudit.ToList();
    }

    /*public List<CFMTrans> GetByName(string prName)
    {
        var linq = from CFMTransactions in _context.CFMTransactions select CFMTransactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMCommitmentAudit Save(CFMCommitmentAudit prCFMCommitmentAudit)
    {
        _context.CFMCommitmentAudit.Add(prCFMCommitmentAudit);
        _context.SaveChanges();

        return prCFMCommitmentAudit;
    }

    public CFMCommitmentAudit Update(CFMCommitmentAudit prCFMCommitmentAudit)
    {
        CFMCommitmentAudit lCFMTransFromDB = _context.CFMCommitmentAudit.First(x => x.ReferenceNumber == prCFMCommitmentAudit.ReferenceNumber);
        _context.Entry(lCFMTransFromDB).CurrentValues.SetValues(prCFMCommitmentAudit);
        _context.SaveChanges();

        return prCFMCommitmentAudit;
    }

    public void Delete(CFMCommitmentAudit prCFMCommitmentAudit)
    {
        _context.Entry(prCFMCommitmentAudit).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}