﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMCommitmentDetailsAuditService : ICFMCommitmentDetailsAuditService
{
    public AppDataContext _context { get; set; }
    public CFMCommitmentDetailsAuditService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMCommitmentDetailsAudit> GetAll()
    {
        return _context.CFMCommitmentDetailsAudit.ToList();
    }

    /*public List<CFMTrans> GetByName(string prName)
    {
        var linq = from CFMTransactions in _context.CFMTransactions select CFMTransactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMCommitmentDetailsAudit Save(CFMCommitmentDetailsAudit prCFMCommitmentDetailsAudits)
    {
        _context.CFMCommitmentDetailsAudit.Add(prCFMCommitmentDetailsAudits);
        _context.SaveChanges();

        return prCFMCommitmentDetailsAudits;
    }

    public CFMCommitmentDetailsAudit Update(CFMCommitmentDetailsAudit prCFMCommitmentDetailsAudits)
    {
        CFMCommitmentDetailsAudit lCFMTransFromDB = _context.CFMCommitmentDetailsAudit.First(x => x.ReferenceNumber == prCFMCommitmentDetailsAudits.ReferenceNumber);
        _context.Entry(lCFMTransFromDB).CurrentValues.SetValues(prCFMCommitmentDetailsAudits);
        _context.SaveChanges();

        return prCFMCommitmentDetailsAudits;
    }

    public void Delete(CFMCommitmentDetailsAudit prCFMCommitmentDetailsAudits)
    {
        _context.Entry(prCFMCommitmentDetailsAudits).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}