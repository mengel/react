﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMCommitmentDetailService : ICFMCommitmentDetailService
{
    public AppDataContext _context { get; set; }
    public CFMCommitmentDetailService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMCommitmentDetails> GetAll()
    {
        return _context.CFMCommitmentDetails.ToList();
    }

    /*public List<CFMTrans> GetByName(string prName)
    {
        var linq = from CFMTransactions in _context.CFMTransactions select CFMTransactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMCommitmentDetails Save(CFMCommitmentDetails prCFMCommitmentDetails)
    {
        _context.CFMCommitmentDetails.Add(prCFMCommitmentDetails);
        _context.SaveChanges();

        return prCFMCommitmentDetails;
    }

    public CFMCommitmentDetails Update(CFMCommitmentDetails prCFMCommitmentDetails)
    {
        CFMCommitmentDetails lCFMTransFromDB = _context.CFMCommitmentDetails.First(x => x.ReferenceNumber == prCFMCommitmentDetails.ReferenceNumber);
        _context.Entry(lCFMTransFromDB).CurrentValues.SetValues(prCFMCommitmentDetails);
        _context.SaveChanges();

        return prCFMCommitmentDetails;
    }

    public void Delete(CFMCommitmentDetails prCFMCommitmentDetails)
    {
        _context.Entry(prCFMCommitmentDetails).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}