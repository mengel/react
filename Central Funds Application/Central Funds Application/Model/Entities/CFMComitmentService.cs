﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMCommitmentService : ICFMCommitmentService
{
    public AppDataContext _context { get; set; }
    public CFMCommitmentService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMCommitment> GetAll()
    {
        return _context.CFMCommitment.ToList();
    }

    /*public List<CFMTrans> GetByName(string prName)
    {
        var linq = from CFMTransactions in _context.CFMTransactions select CFMTransactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMCommitment Save(CFMCommitment prCFMCommitment)
    {
        _context.CFMCommitment.Add(prCFMCommitment);
        _context.SaveChanges();

        return prCFMCommitment;
    }

    public CFMCommitment Update(CFMCommitment prCFMCommitment)
    {
        CFMCommitment lCFMTransFromDB = _context.CFMCommitment.First(x => x.ReferenceNumber == prCFMCommitment.ReferenceNumber);
        _context.Entry(lCFMTransFromDB).CurrentValues.SetValues(prCFMCommitment);
        _context.SaveChanges();

        return prCFMCommitment;
    }

    public void Delete(CFMCommitment prCFMCommitment)
    {
        _context.Entry(prCFMCommitment).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}