﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Central_Funds_Application.Model.Entities;

[Table("CFM_Commitment")]
public class CFMCommitment
{
    public string ReferenceNumber { get; set; }
    public string SequenceNumber { get; set; }
    public DateTime EntryDate { get; set; }
    public DateTime UpdateDate { get; set; }
    public string FiscalYearStart { get; set; }
    public string FiscalYearEnd { get; set; }
    public string DistributionType { get; set; }
    public string BudgetActualFlag { get; set; }

}
