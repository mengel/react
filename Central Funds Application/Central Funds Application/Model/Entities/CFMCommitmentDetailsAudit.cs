﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Central_Funds_Application.Model.Entities;

[Table("CFM_Commitment_Details_Audit")]
public class CFMCommitmentDetailsAudit
{
    public string ReferenceNumber { get; set; }
    public string SequenceNumber { get; set; }
    public DateTime EntryDate { get; set; }
    public DateTime UpdateDate { get; set; }
    public string Fisicalyear { get; set; }
    public string TransactionTyepe { get; set; }
    public string Fund { get; set; }
    public string FromVP { get; set; }
    public string FromAccountNumber { get; set; }
    public int TotalAmount { get; set; }
    public string toVP { get; set; }
    public string ToAccountNumber  { get; set; }
   
}
