﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMDistributionTypeService : ICFMDistributionTypeService
{
    public AppDataContext _context { get; set; }
    public CFMDistributionTypeService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMDistributionType> GetAll()
    {
        return _context.CFMDistributionType.ToList();
    }
}