﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMInstitutionalPrioritiesService : ICFMInstitutionalPrioritiesService
{
    public AppDataContext _context { get; set; }
    public CFMInstitutionalPrioritiesService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMInstitutionalPriorities> GetAll()
    {
        return _context.CFMInstitutionalPriorities.ToList();
    }
}