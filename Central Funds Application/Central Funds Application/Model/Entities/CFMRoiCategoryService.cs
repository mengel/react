﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMRoiCategoryService : ICFMRoiCategoryService
{
    public AppDataContext _context { get; set; }
    public CFMRoiCategoryService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMRoiCategory> GetAll()
    {
        return _context.CFMRoiCategory.ToList();
    }
}