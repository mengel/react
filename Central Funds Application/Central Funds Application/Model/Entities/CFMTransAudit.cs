﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Central_Funds_Application.Model.Entities;

[Table("CFM_Trans_Audit")]
public class CFMTransAudit
{
    public string ReferenceNumber { get; set; }
    public string SequenceNumber { get; set; }
    public DateTime EntryDate { get; set; }
    public DateTime UpdateDate { get; set; }
    public DateTime? LastReportedSeqDate { get; set; }
    public string ShortDescription { get; set; }
    public string Category { get; set; }
    public string DistributionType { get; set; }
    public string ReviewFlag { get; set; }
    public string HistoricalReferenceNumber { get; set; }
    public string LongDescription { get; set; }
    public string Fund { get; set; }
    public int? TotalAmount { get; set; }
    public string FiscalYearStart { get; set; }
    public string FisicalYearEnd { get; set; }
    public string InstitutionalPriorities { get; set; }
    public string InstitutionalPrioritiesExplained { get; set; }
    public string InstitutionalRef { get; set; }
    public string ReturnOnInvestment { get; set; }
    public string ReturnOnInvestmentDesc { get; set; }
    public string ReportValidRoi { get; set; }
    public string Roitype { get; set; }
    public string Roiref { get; set; }
    public string Roi { get; set; }
    public string ReportCriteria { get; set; }
    public string FilesRef { get; set; }
    public string FormStatus { get; set; }
    public string DecisionLogNumber { get; set; }
    public string User { get; set; }
}
