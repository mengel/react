﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMTransAuditService : ICFMTransAuditService
{
    public AppDataContext _context { get; set; }
    public CFMTransAuditService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMTransAudit> GetAll()
    {
        return _context.CFMTransAudit.ToList();
    }

    /*public List<CFMTransAudit> GetByName(string prName)
    {
        var linq = from CFMTransAuditactions in _context.CFMTransAuditactions select CFMTransAuditactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMTransAudit Save(CFMTransAudit prCFMTransAudit)
    {
        _context.CFMTransAudit.Add(prCFMTransAudit);
        _context.SaveChanges();

        return prCFMTransAudit;
    }

    public CFMTransAudit Update(CFMTransAudit prCFMTransAudit)
    {
        CFMTransAudit lCFMTransAuditFromDB = _context.CFMTransAudit.First(x => x.ReferenceNumber == prCFMTransAudit.ReferenceNumber);
        _context.Entry(lCFMTransAuditFromDB).CurrentValues.SetValues(prCFMTransAudit);
        _context.SaveChanges();

        return prCFMTransAudit;
    }

    public void Delete(CFMTransAudit prCFMTransAudit)
    {
        _context.Entry(prCFMTransAudit).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}