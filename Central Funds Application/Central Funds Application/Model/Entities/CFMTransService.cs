﻿using React_Asp;

namespace Central_Funds_Application.Model.Entities;
public class CFMTransService : ICFMTransService
{
    public AppDataContext _context { get; set; }
    public CFMTransService(AppDataContext prAppDataContext)
    {
        _context = prAppDataContext;
    }

    public List<CFMTrans> GetAll()
    {
        return _context.CFMTransactions.ToList();
    }

    /*public List<CFMTrans> GetByName(string prName)
    {
        var linq = from CFMTransactions in _context.CFMTransactions select CFMTransactions;

        if (!string.IsNullOrWhiteSpace(prName))
            linq = linq.Where(x => x.Name.ToUpper().Contains(prName.ToUpper()));

        return linq.ToList();
    }*/

    public CFMTrans Save(CFMTrans prCFMTrans)
    {
        _context.CFMTransactions.Add(prCFMTrans);
        _context.SaveChanges();

        return prCFMTrans;
    }

    public CFMTrans Update(CFMTrans prCFMTrans)
    {
        CFMTrans lCFMTransFromDB = _context.CFMTransactions.First(x => x.ReferenceNumber == prCFMTrans.ReferenceNumber);
        _context.Entry(lCFMTransFromDB).CurrentValues.SetValues(prCFMTrans);
        _context.SaveChanges();

        return prCFMTrans;
    }

    public void Delete(CFMTrans prCFMTrans)
    {
        _context.Entry(prCFMTrans).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _context.SaveChanges();
    }
}