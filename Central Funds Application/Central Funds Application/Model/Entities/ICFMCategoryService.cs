﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMCategoryService
    {
        List<CFMCategory> GetAll();
    }
}