﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMCommitmentAuditService
    {
        void Delete(CFMCommitmentAudit prLibrary);
        List<CFMCommitmentAudit> GetAll();
        //List<CFMTrans> GetByName(string prName);
        CFMCommitmentAudit Save(CFMCommitmentAudit prLibrary);
        CFMCommitmentAudit Update(CFMCommitmentAudit prLibrary);
    }
}