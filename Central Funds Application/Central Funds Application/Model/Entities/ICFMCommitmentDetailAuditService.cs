﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMCommitmentDetailsAuditService
    {
        void Delete(CFMCommitmentDetailsAudit prLibrary);
        List<CFMCommitmentDetailsAudit> GetAll();
        //List<CFMTrans> GetByName(string prName);
        CFMCommitmentDetailsAudit Save(CFMCommitmentDetailsAudit prLibrary);
        CFMCommitmentDetailsAudit Update(CFMCommitmentDetailsAudit prLibrary);
    }
}