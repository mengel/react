﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMCommitmentDetailService
    {
        void Delete(CFMCommitmentDetails prLibrary);
        List<CFMCommitmentDetails> GetAll();
        //List<CFMTrans> GetByName(string prName);
        CFMCommitmentDetails Save(CFMCommitmentDetails prLibrary);
        CFMCommitmentDetails Update(CFMCommitmentDetails prLibrary);
    }
}