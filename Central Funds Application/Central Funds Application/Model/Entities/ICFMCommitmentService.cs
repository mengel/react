﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMCommitmentService
    {
        void Delete(CFMCommitment prLibrary);
        List<CFMCommitment> GetAll();
        //List<CFMTrans> GetByName(string prName);
        CFMCommitment Save(CFMCommitment prLibrary);
        CFMCommitment Update(CFMCommitment prLibrary);
    }
}