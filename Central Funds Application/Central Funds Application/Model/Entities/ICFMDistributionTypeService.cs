﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMDistributionTypeService
    {
        List<CFMDistributionType> GetAll();
    }
}