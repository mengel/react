﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMInstitutionalPrioritiesService
    {
        List<CFMInstitutionalPriorities> GetAll();
    }
}