﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMTransAuditService
    {
        void Delete(CFMTransAudit prLibrary);
        List<CFMTransAudit> GetAll();
        //List<CFMTransAudit> GetByName(string prName);
        CFMTransAudit Save(CFMTransAudit prLibrary);
        CFMTransAudit Update(CFMTransAudit prLibrary);
    }
}