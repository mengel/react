﻿namespace Central_Funds_Application.Model.Entities
{
    public interface ICFMTransService
    {
        void Delete(CFMTrans prLibrary);
        List<CFMTrans> GetAll();
        //List<CFMTrans> GetByName(string prName);
        CFMTrans Save(CFMTrans prLibrary);
        CFMTrans Update(CFMTrans prLibrary);
    }
}