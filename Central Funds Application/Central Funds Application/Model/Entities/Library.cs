﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Central_Funds_Application.Model.Entities;

[Table("Library")]
public class Library
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int Id { get; set; }
    [Required]
    public String Name { get; set; }
    [Required]
    public String Address { get; set; }
    public String Telephone { get; set; }
}