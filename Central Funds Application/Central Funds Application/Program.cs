using Central_Funds_Application;
using Central_Funds_Application.Model.Entities;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddScoped<ILibraryService, LibraryService>();
builder.Services.AddScoped<ICFMTransService, CFMTransService>();
builder.Services.AddScoped<ICFMRoiCategoryService, CFMRoiCategoryService>();
builder.Services.AddScoped<ICFMCategoryService, CFMCategoryService>();
builder.Services.AddScoped<ICFMDistributionTypeService, CFMDistributionTypeService>();
builder.Services.AddScoped<ICFMInstitutionalPrioritiesService, CFMInstitutionalPrioritiesService>();
builder.Services.AddScoped<ICFMCommitmentService, CFMCommitmentService>();
builder.Services.AddScoped<ICFMCommitmentDetailService, CFMCommitmentDetailService>();

builder.Services.AddScoped<ICFMCommitmentAuditService, CFMCommitmentAuditService>();
builder.Services.AddScoped<ICFMCommitmentDetailsAuditService, CFMCommitmentDetailsAuditService>();
builder.Services.AddScoped<ICFMTransAuditService, CFMTransAuditService>();



builder.Services.AddDbContext<AppDataContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("DBConnect")));
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "ASP.NET CORE API", Version = "v1" });
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<AppDataContext>();
    context.Database.Migrate();
}


app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseCors(x => x.AllowAnyMethod().AllowAnyHeader().SetIsOriginAllowed(origin => true));
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "React ASP.NET");
});

app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.Run();
